/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React from 'react';
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import imageReducer from './src/redux/reducers/imageReducer' 
import nameReducer from './src/redux/reducers/nameReducer' 

const rootReducer = combineReducers({
    imageStore: imageReducer, 
    nameStore: nameReducer})

const store = createStore(rootReducer);

const AppContainer = () => (
    <Provider store={store}>
        <App />
    </Provider>
)

AppRegistry.registerComponent(appName, () => AppContainer);
