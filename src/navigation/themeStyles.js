export const BACKGROUND_MAIN_COLOR = '#262626';
export const THEME_COLOR = '#63eaff'
export const TEXT_MAIN_COLOR = '#000000';
export const TEXT_WHITE_COLOR = '#f4f4f4';
export const DISABLED_BUTTON = '#adadad';