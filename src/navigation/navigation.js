import React, { Component } from 'react';
import { Scene, Router, Stack } from 'react-native-router-flux'
import animation from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator'
import Login from './../screens/login/index'
import Main from './../screens/main/index'
import Splash from './../screens/splash/index'
import * as theme from './themeStyles'

class Navigation extends Component {
    state = {}
    render() {
        return (
            <Router sceneStyle={{backgroundColor: theme.BACKGROUND_MAIN_COLOR}}>
                <Stack key="root" tabs={false} legacy={true} lazy={true} transitionConfig={() => ({screenInterpolator: animation.forHorizontal})}> 

                    <Scene key="login"
                        component={Login}
                        hideNavBar />

                    <Scene key="main"
                        component={Main}
                        hideNavBar 
                        />

                    <Scene key="splash"
                        initial
                        component={Splash}
                        hideNavBar />

                </Stack>
            </Router>
        );
    }
}

export default Navigation;