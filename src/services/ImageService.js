class ImageService {
    static getImage = (success, error) => {
        fetch('https://source.unsplash.com/random')
            .then((response) => {
                success(response.url);
            })
            .catch((errorMessage) => {
                error(errorMessage);
            })
    }
}

export default ImageService;