import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import Present from "./present";
import { setFullname } from './../../redux/actions/nameActions'

class Main extends Component {
    
    handleUpdate(firstName, lastName) {
        this.props.setFullname(firstName, lastName);
    }

    render() {
        return (
            <Present imageUrl={this.props.imageUrl}
                firstName={this.props.firstName}
                lastName={this.props.lastName}
                handleUpdate={(first, last) => this.handleUpdate(first, last)}/>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        imageUrl: state.imageStore.imageUrl,
        firstName: state.nameStore.firstName,
        lastName: state.nameStore.lastName
    }
}

const actions = {
    setFullname
}

export default connect(mapStateToProps, actions)(Main);