import { StyleSheet } from 'react-native'
import { Dimensions } from 'react-native';
import * as theme from './../../navigation/themeStyles'

export default styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: '15%',
    },
    blockWrapper: {
        width:'100%'
    },
    image: {
        borderWidth:3,
        borderColor: theme.THEME_COLOR,
        width: Dimensions.get('window').width*0.7,
        height: Dimensions.get('window').height*0.35,
        borderRadius:10,
    },
    input: {
        width:'100%',
        backgroundColor:'white',
        marginVertical:15,
    },
    button: {
        width:'100%',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:15,
        display: 'flex',
        flexDirection: 'row'
    },
    textButton: {
        marginHorizontal:5,
        color: theme.TEXT_MAIN_COLOR
    }
});