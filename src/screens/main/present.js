import React, { Component } from 'react';
import { View, Text, TextInput, TouchableHighlight, Image, Alert } from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import Icon from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient';

import * as theme from './../../navigation/themeStyles'
import styles from "./styles";

class Present extends Component {

    state = {
        firstName: this.props.firstName,
        lastName: this.props.lastName
    }

    nextHandler() {
        this.props.handleUpdate(this.state.firstName, this.state.lastName);
        Alert.alert("Success","Data was updated");
    }

    isDisabled() {
        let disableButton = true;
        if (this.state.firstName != '' && this.state.lastName != '') {
            disableButton = false;
        }
        return disableButton;
    }

    render() {
        return (
            <KeyboardAwareView>
                <View style={styles.wrapper}>
                    <View style={styles.blockWrapper}>
                        <Image
                            source={{ uri: this.props.imageUrl }}
                            style={styles.image}
                        />
                    </View>
                    <View style={styles.blockWrapper}>
                        <TextInput
                            style={styles.input}
                            placeholder='First Name'
                            onChangeText={(text) => this.setState({ firstName: text })}
                            value={this.state.firstName} />
                        <TextInput
                            style={styles.input}
                            placeholder='Last Name'
                            onChangeText={(text) => this.setState({ lastName: text })}
                            value={this.state.lastName} />
                    </View>
                    <View style={styles.blockWrapper}>
                        <TouchableHighlight
                            onPress={this.nextHandler.bind(this)}
                            disabled={this.isDisabled()} >
                            <LinearGradient 
                                style={styles.button}
                                colors={this.isDisabled() ? 
                                    ['grey', 'grey'] :
                                    [theme.THEME_COLOR, 'violet']}
                                >
                                <Icon name='star' size={20} color={theme.TEXT_MAIN_COLOR}/>
                                <Text style={styles.textButton}>
                                    {'Update'.toUpperCase()}
                                </Text>
                            </LinearGradient>
                        </TouchableHighlight>
                    </View>
                </View>
            </KeyboardAwareView>
        );
    }
}

export default Present;