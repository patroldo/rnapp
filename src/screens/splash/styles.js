import { StyleSheet, Dimensions } from 'react-native'

export default styles = StyleSheet.create({
    wrap: {
        flex: 1
    },
    image: {
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height
    }
});