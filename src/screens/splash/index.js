import React, { Component } from 'react'
import { connect } from 'react-redux'
import Present from "./present";
import { Actions } from 'react-native-router-flux'
import { setImage } from './../../redux/actions/imageActions'
import imageService from './../../services/ImageService'
import ImageService from './../../services/ImageService';

class Splash extends Component {

    componentWillMount() {
        this.successGetImage = this.successGetImage.bind(this);
        ImageService.getImage( 
            (url) => { this.successGetImage(url) }, 
            (error) => {
                console.log('****');
                console.log(error);
                console.log('****');
                alert("Sorry, Something went wrong");
            }
        );
    }

    successGetImage(url) {
        this.props.setImage(url);
        Actions.jump('login');
    }

    render() {
        return (
            <Present />
        );
    }
}

export default connect(null, { setImage })(Splash);