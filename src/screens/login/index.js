import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import Present from "./present";
import { setFullname } from './../../redux/actions/nameActions'
import imageReducer from '../../redux/reducers/imageReducer'

class Login extends Component {

    nextHandler(firstName, lastName) {
        this.props.setFullname(firstName, lastName);
        Actions.jump('main', {});
    }

    render() {
        return (
            <Present 
                imageUrl={this.props.imageUrl}
                nextHandler={(fisrt, last) => this.nextHandler(fisrt,last)} />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        imageUrl: state.imageStore.imageUrl
    }
}

const actions = {
    setFullname
}

export default connect(mapStateToProps, actions)(Login);