import * as types from './../types/imageTypes';
import initState from './../initStates/imageInitialState'

export default (state = initState, action) => {
    switch(action.type) {
        case types.SET_IMAGE: {
            state.imageUrl = action.imageUrl;
            break;
        }
    }
    return { ...state };
}