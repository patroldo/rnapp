import * as types from './../types/nameTypes';
import initState from './../initStates/nameInitialState'

export default (state = initState, action) => {
    switch(action.type) {
        case types.SET_FULLNAME: {
            state.firstName = action.firstName;
            state.lastName = action.lastName;
            break;
        }
        case types.SET_FIRSTNAME: {
            state.firstName = action.firstName;
            break;
        }
        case types.SET_LASTNAME: {
            state.lastName = action.lastName;
            break;
        }
    }
    return state;
}