import * as types from './../types/imageTypes'

export const setImage = (imageUrl) => {
    return {
        type: types.SET_IMAGE,
        imageUrl
    }
}