import * as types from './../types/nameTypes'

export const setFullname = (firstName, lastName) => {
    return {
        type: types.SET_FULLNAME,
        firstName,
        lastName
    }
}

export const setFirstname = (firstName) => {
    return {
        type: types.SET_FIRSTNAME,
        firstName
    }
}

export const setLastname = (lastName) => {
    return {
        type: types.SET_IMAGE,
        lastName
    }
}
